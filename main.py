# -*- coding: utf-8 -*-
from html_to_docx import docx_creator
import tldextract
import sys

end = None
url = "https://www.novels.pl/novel/Super-God-Gene/{}/Chapter-2333--Water-Fairy.html\n"
name = ''

def main():
    # name = "name"
    # #fill_urls()

    # with open("urls.txt", 'r') as f:
    #     urls = f.read().splitlines()

    # if urls:
    #     end = get_end(urls[0])
    print(sys.argv)
    docx_creator(urls=sys.argv[3:], end=end).create_docx(sys.argv[1], sys.argv[2])
    # docx_creator(urls=urls, end=end).create_docx(name)

def fill_urls():
    '''Change content according to the website'''

    open("urls.txt", 'w').close()
    with open("urls.txt", 'w') as f:
        for i in range(1600, 2334):
            f.write(url.format(i))

def get_end(url):
    '''Returns known end of the website's articles'''

    url_info = tldextract.extract(url)
    domain = url_info.registered_domain

    if "novels.pl" == domain:
        return "Liked it? Take a second to support Novels on Patreon!"
    else:
        return None

if __name__ == "__main__":
    main()

