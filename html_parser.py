# -*- coding: utf-8 -*-
import imgkit
import requests
import html
import docx
import time
import os

from bs4 import BeautifulSoup 
from docx.shared import Cm
from docx.shared import Pt
from docx.shared import RGBColor
from docx.enum.text import WD_BREAK
from readability import Document
from PIL import Image
from enum import Enum     

INFINITY = 0x4f4f4f4f
IMAGE_PATH = "temp_images/image-to-return{}.png"

class Docx_Creator():
    def __init__(self, url_file, tables_to_image=False, start='', end=''):
        self.start = start
        self.end = end
        self.tables_to_image = tables_to_image
        self.file_path = url_file

    def create_docx(self, name=''):
        doc = docx.Document()
        paragraph_format = doc.styles['Normal'].paragraph_format
        paragraph_format.space_before = Pt(10)
        paragraph_format.space_before = Pt(10)

        with open(self.file_path, 'r') as f:
            urls = f.read().split('\n')

        for url in urls:
            content = self.get_html(url, self.start, self.end)
            text, indexes = self.parse_html(content)
            self.edit_docx(text, indexes, doc)
        
        if name:
            doc.save(name)
        else:
            doc.save('out/Articles{}.docx'.format(time.strftime("%H_%M_%S", time.localtime())))

    def edit_docx(self, text, indexes, doc):
        paragraphs = text.split("\n")

        heading = doc.add_heading(html.unescape(paragraphs[0]), 1)
        heading.alignment = 1

        p_index = []
        for p in paragraphs:
            p_index.append(text.find(p))

        i = 0
        p = 1

        while p < len(paragraphs):
            paragraph = paragraphs[p]
            if p < len(p_index):
                index = p_index[p-1]
            else:
                index = INFINITY

            if self.tables_to_image and (i < len(indexes)):
                if(index >= indexes[i]):
                    pr = doc.add_paragraph()
                    r = pr.add_run()
                    r.add_picture(IMAGE_PATH.format(i), width=Cm(15.0))
                    pr.alignment = 1
                    i += 1
                else:
                    self.format_paragraph(doc, paragraph)
                    p += 1
            else:
                self.format_paragraph(doc, paragraph)
                p += 1

    def format_paragraph(self, doc, text):
        # Change special characters
        text = text.replace("…..", "...")
        text = "    " + text
        formats = []
        res_s = []
        res_e = []

        # Check for dialogs
        if (text.find("“") != -1):
            start = text.find("“")
            end = text.find("”")
            formats.append((start, end+1, 'bi'))

        # Check for thoughts
        word_ends = ['s', 'v', 't', 'm', 'l', 'd', 'r']
        res_s = self.find_occurrences(text, "‘")

        if len(res_s) != 0:
            res_e = self.find_occurrences(text, "’")
            i = 0
            for j in range(0,len(res_e)):
                if i == len(res_s):
                    break

                if (j+1 < len(res_e)) and not (text[res_e[j]+1] in word_ends) and text[res_e[j]-1] != 's':   
                    formats.append((res_s[i], res_e[j]+1, 'bi'))
                    i += 1
                elif i < len(res_s) and j+1 == len(res_e):
                    formats.append((res_s[i], res_e[j]+1, 'bi'))
                    i += 1

        # Check for tags
        self.format_between(text, '<', '>', 'i', formats)
        self.format_between(text, './', '\\.', 'b', formats)

        # res_s = self.find_occurrences(text, "<")    
        # if res_s.count != 0:
        #     res_e = self.find_occurrences(text, ">")
        #     if len(res_e) != 0:
        #         for i in range(0,len(res_s)):
        #             formats.append((res_s[i], res_e[i]+1, 'i'))
    
        # Format doc file
        p_text = doc.add_paragraph()

        current_index = 0
        formats.sort(key=lambda tup: tup[0])

        for i in range(0, len(formats)):
            cur_format = formats[i]

            if current_index <= cur_format[0]:
                p_text.add_run(text[current_index:cur_format[0]])
                current_index = cur_format[1]
            else:
                continue

            r = p_text.add_run(text[cur_format[0]:cur_format[1]])

            if 'b' in cur_format[2]:
                r.bold = True
            if 'i' in cur_format[2]:
                r.italic = True

        p_text.add_run(text[current_index:])

    def find_occurrences(self, str_, char):
        return [i for i, letter in enumerate(str_) if letter == char]

    def format_between(self, text, start, end, style, formats):
        res_s = self.find_occurrences(text, start)  
        if res_s.count != 0:
            res_e = self.find_occurrences(text, end)
            if len(res_e) != 0:
                for i in range(0,len(res_s)):
                    formats.append((res_s[i], res_e[i]+1, style))

    def parse_html(self, html_string, all_tags = False): 
        if not all_tags: 
            html_string = self.parse_breaks(html_string)

        i = 0
        tables_indexes = []

        while (html_string.find('>') != -1):
            tag_start = html_string.find('<')
            tag_end = html_string.find('>')

            if not all_tags and "<script" in html_string[tag_start: tag_end+1]:
                script_end = html_string.find('</script>')
                html_string = html_string.replace(html_string[tag_start: script_end+9], "")
            elif not all_tags and "<table" in html_string[tag_start: tag_end+1]:
                table_end = html_string.find('</table>')

                self.create_table_image(html_string[tag_start:table_end+8], i)
                html_string = html_string.replace(html_string[tag_start:table_end+8], '')
                tables_indexes.append(tag_start+1)
            else:
                html_string = html_string.replace(html_string[tag_start: tag_end+1], "")
            

        return html_string, tables_indexes

    def parse_breaks(self, html_string):    
        html_string = html_string.replace("\\n", "\n")
        html_string = html_string.replace("<br>", "\n")
        html_string = html_string.replace("<br/>", "\n")
        html_string = html_string.replace("</p><p>", "\n")

        return html_string

    #Gets text from url from and to specified words
    def get_html(self, url, start_word = '', end_word = ''):
        #Get text
        responce = requests.get(url)
        chapter_html = responce.text
        chapter_html = Document(chapter_html).summary()

        soup = BeautifulSoup(chapter_html, 'html5lib')
        article_soup = soup.find('div', attrs = {'id':'chr-content'})
        rows = []

        for br in soup.find_all("br"):
            br.replace_with("\n" + br.text)

        for row in article_soup.contents:             
            if row.name == 'table':
                # if self.tables_to_image:
                self.create_table_image(row, 1)
                # else:
                # rows.append(self.create_table_string(row))
            elif row.name == 'p':
                rows.append(row.text)

        a = Document(chapter_html).short_title()

        chapter_html = Document(chapter_html).content()

        if start_word:
            start = chapter_html.lower().find(start_word.lower())
            chapter_html = chapter_html[start:]

        if end_word:
            finish = chapter_html.lower().rfind(end_word.lower())
            chapter_html = chapter_html[:finish]

        chapter_html = a + '\n' + chapter_html

        return chapter_html

    #Creates image of table
    def create_table_image(self, table, index):
        html_table = "<html>" + table + "</html>"

        html_table = html.escape(html_table).encode('ascii', 'xmlcharrefreplace')
        html_table = html_table.replace("\n", "<br/>")

        # html_table = html.escape(html_table, quote=True)
        
        options = {
            'format': 'png'
        }

        css = "table.css"
        imgkit.from_string(html_table, IMAGE_PATH.format(index), options=options, css=css)
        self.compress_me(IMAGE_PATH.format(index), 5, True)

    def create_table_string(self, table):    
        line = "——————————————————————————————————————-"
        new_table = [line]

        for row in table.findAll('tr'):
            for col in row.findAll('td'):
                if col.find('strong'):
                    bold = True
                # if col.find('br'):

                new_table.append(col.contents[:])

                # new_table.append('./' + col + '\\.')
            # new_table[-1] = new_table[-1]

        new_table.append(line)

        return '\n'.join(new_table)        

    def compress_me(self, file, quality, verbose=False):
        filepath = os.path.join(os.getcwd(), file)
        oldsize = os.stat(filepath).st_size
        picture = Image.open(filepath)

        picture = picture.quantize(colors=16, method=2)  
        picture.save(file,"PNG",optimize=True,quality=quality) 

        newsize = os.stat(os.path.join(os.getcwd(),file)).st_size
        percent = (oldsize-newsize)/float(oldsize)*100
        if verbose:
            print("File compressed from {0} to {1} or {2}%".format(oldsize,newsize,percent))