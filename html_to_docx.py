# -*- coding: utf-8 -*-
import imgkit
import requests
import html
import docx
import time
import os
import sys

from bs4 import BeautifulSoup 
from docx.shared import Cm
from docx.shared import Pt
from docx.shared import RGBColor
from docx.enum.text import WD_BREAK
from readability import Document
from PIL import Image
from pathlib import Path

INFINITY = 0x4f4f4f4f
IMAGE_PATH = "temp_images/image-to-return{}.png"

class docx_creator():
    def __init__(self, url='', urls=[], tables_to_image=False, start='', end='', end_included=False):
        self.start = start
        self.end = end
        self.end_included = end_included
        self.tables_to_image = tables_to_image
        self.url = url
        self.urls = urls

    # Creates docx document based on provided links
    def create_docx(self, name='', folder=''):
        doc = docx.Document()
        paragraph_format = doc.styles['Normal'].paragraph_format
        paragraph_format.space_before = Pt(10)
        paragraph_format.space_before = Pt(10)
        
        if self.urls:
            for url in self.urls:
                self.add_article(doc, url)
        elif self.url:
            self.add_article(doc, self.url)
        else:
            return

        if name:
            path = Path('{}/out/{}.docx'.format(folder,name))
            path.parent.mkdir(parents=True, exist_ok=True)
            doc.save('{}/out/{}.docx'.format(folder,name))
        else:
            path = Path('out/Article{}.docx'.format(time.strftime("%H_%M_%S", time.localtime())))
            path.parent.mkdir(parents=True, exist_ok=True)
            doc.save('out/Article{}.docx'.format(time.strftime("%H_%M_%S", time.localtime())))

    # Adds article to the document 
    def add_article(self, doc, url):        
        title, article_html = self.get_html(url)

        if self.tables_to_image:
            article_rows, table_indexes = self.format_html_article(article_html)
            self.extend_docx(doc, title, article_rows, indexes=table_indexes)
        else:
            article_rows = self.format_html_article(article_html)
            self.extend_docx(doc, title, article_rows)

        # print("{} added".format(title))

    # Gets html content from the url
    def get_html(self, url):
        responce = requests.get(url)
        article_html = responce.text
        title = Document(article_html).short_title()
        article_html = Document(article_html).summary()
        article_html = Document(article_html).content()

        return title, article_html

    # Extracts only needed information inside given html
    def format_html_article(self, article_html):
        if self.tables_to_image:
            i = 0
            table_indexes = []

        soup = BeautifulSoup(article_html, 'html5lib')
        tag = None
        tag_content = 0
        
        #TODO: change "try - except" to proper validation
        for _tag in soup.descendants:
            try:
                if _tag.contents:
                    a = 1
            except: 
                continue

            tmp = len(_tag.contents)
            if (tmp > tag_content):
                tag = _tag
                tag_content = tmp

        article_rows = []

        for row in tag.contents:
            if row.name == 'table':
                if self.tables_to_image:
                    self.table_to_image(row, i)
                    table_indexes.append(len(article_rows) - 1)
                    i += 1
                else:
                    article_rows.extend(self.table_to_string(row))
                continue

            try:
                text = self.format_html_tag(row).text
                if text:                    
                    if self.end and self.end in text:
                        if self.end_included:
                            article_rows.append(text)
                        break
                    else:
                        article_rows.append(text)
            except:
                text = self.format_html_string(row)
                if text:
                    if self.end and self.end in text:
                        if self.end_included:
                            article_rows.append(text)
                        break
                    else:
                        article_rows.append(text)

        if self.tables_to_image:
            return article_rows, table_indexes
        else:
            return article_rows
    
    #Filling new article with content
    def extend_docx(self, doc, title, article_rows, indexes=[]):
        heading = doc.add_heading(title, 1)
        heading.alignment = 1

        for row in article_rows:
            self.add_paragraph(doc, row)
    
    # Formates and adds paragraph to the docx document 
    def add_paragraph(self, doc, text):
        # Change special characters
        text = text.replace("…..", "...")
        text = "    " + text
        formats = []
        wrapper_start = []
        wrapper_end = []

        # Check for dialogs
        if (text.find("“") != -1):
            start = text.find("“")
            end = text.find("”")
            formats.append((start, end+1, 'bi'))

        # Check for thoughts
        word_ends = ['s', 'v', 't', 'm', 'l', 'd', 'r']
        wrapper_start = list(self.find_all(text, "‘"))

        if len(wrapper_start) != 0:
            wrapper_end = list(self.find_all(text, "’"))
            i = 0
            for j in range(0,len(wrapper_end)):
                if i == len(wrapper_start):
                    break

                if (j+1 < len(wrapper_end)) and not (text[wrapper_end[j]+1] in word_ends) and text[wrapper_end[j]-1] != 's':   
                    formats.append((wrapper_start[i], wrapper_end[j]+1, 'bi'))
                    i += 1
                elif i < len(wrapper_start) and j+1 == len(wrapper_end):
                    formats.append((wrapper_start[i], wrapper_end[j]+1, 'bi'))
                    i += 1

        # Check for wrappers
        text = self.format_between(text, '<', '>', 'i', formats)
        text = self.format_between(text, ';*', '*;', 'b', formats, delete_wrappers=True)
    
        # Format doc file
        p_text = doc.add_paragraph()

        current_index = 0
        formats.sort(key=lambda tup: tup[0])

        for i in range(0, len(formats)):
            cur_format = formats[i]

            if current_index <= cur_format[0]:
                p_text.add_run(text[current_index:cur_format[0]])
                current_index = cur_format[1]
            else:
                continue

            r = p_text.add_run(text[cur_format[0]:cur_format[1]])

            if 'b' in cur_format[2]:
                r.bold = True
            if 'i' in cur_format[2]:
                r.italic = True

        p_text.add_run(text[current_index:])

    # Format between given wrappers text
    def format_between(self, text, start, end, style, formats, delete_wrappers=False):
        wrapper_start = list(self.find_all(text, start)) # self.find_occurrences(text, start)  
        if wrapper_start.count != 0:
            wrapper_end = list(self.find_all(text, end))
            if len(wrapper_end) != 0:
                for i in range(0,len(wrapper_start)):
                    if delete_wrappers:
                        formats.append((wrapper_start[i], wrapper_end[i]-len(start), style))
                        text = text[:wrapper_start[i]] + text[wrapper_start[i] + len(start) : wrapper_end[i]] + text[wrapper_end[i] + len(end):]
                    else:
                        try:
                            formats.append((wrapper_start[i], wrapper_end[i] + 1, style))
                        except:
                            return text

        return text

    # Returns all occurrences of substring in string
    def find_all(self, a_str, sub):
        start = 0
        while True:
            start = a_str.find(sub, start)
            if start == -1: return
            yield start
            start += len(sub)
    
    # Changes tag's text and components inside
    def format_html_tag(self, html_tag):
        for br in html_tag.find_all("br"):
            br.replace_with("\n" + br.text)

        for strong in html_tag.find_all("strong"):
            strong.replace_with(";*" + strong.text + "*;")

        for strong in html_tag.find_all("\\n"):
            strong.replace_with()

        for strong in html_tag.find_all("strong"):
            strong.replace_with(";*" + strong.text + "*;")

        # html_text = html_tag.text.replace('\\n', '')
        # html_text = html_text.replace('\\t', '')
            
        return html_tag
    
    # Changes tags and text inside a string to make it more readable
    def format_html_string(self, html_string):
        html_string = html_string.replace('<br/>', '\n')
        html_string = html_string.replace('<br>', '\n')
        html_string = html_string.replace('\\n', '')
        html_string = html_string.replace('\\t', '')

        return html_string

    # TODO: Change line length
    # Converts table to a string based o it content
    def table_to_string(self, table):
        line = "——————————————————————————————————————-"
        # print(table.prettify())
        table = self.format_html_tag(table)

        table_rows = [line]

        # for row in table.findAll('tr'):
        #     tds = table.findAll('td')
        #     if len(tds) > 1:
        for row in table.findAll('td'):
            if row.text:
                table_rows.append(row.text.replace('\n', '\n\n'))

        table_rows.append(line)            

        return table_rows
        
    # Converts table to image
    # TODO: Add image cropping if image if bigger then 22 inches
    def table_to_image(self, table, index):
        table = Document(repr(table)).content()[2:-1]
        html_table = "<html>" + str(table).replace('<br>', '').replace('<br/', '') + "</html>"
        
        options = {
            'format': 'png'
        }

        css = "table.css"
        imgkit.from_string(html_table, IMAGE_PATH.format(index), options=options, css=css)
        self.compress_png(IMAGE_PATH.format(index), 5, text=True, verbose=True)

    # Compresses the image of png extention
    def compress_png(self, file, quality, text=False, verbose=False):
        filepath = os.path.join(os.getcwd(), file)
        oldsize = os.stat(filepath).st_size
        picture = Image.open(filepath)

        if text:
            picture = picture.quantize(colors=16, method=2) 

        picture.save(file,"PNG",optimize=True,quality=quality) 

        newsize = os.stat(os.path.join(os.getcwd(),file)).st_size
        percent = (oldsize-newsize)/float(oldsize)*100
        if verbose:
            print("File compressed from {0} to {1} or {2}%".format(oldsize,newsize,percent))